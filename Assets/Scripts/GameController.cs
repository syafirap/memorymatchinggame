﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	[SerializeField]
	private Sprite bgImage;

	public float[] scores = new float[5];

	public Sprite[] puzzles;

	public List<Sprite> gamePuzzles = new List<Sprite> ();
	public List<Button> btns = new List<Button>();

	public Button restart;

	private bool firstGuess, secondGuess;

	private int countGuesses;
	private int countCorrectGuesses;
	private int gameGuesses;

	private int firstGuessIndex, secondGuessIndex;

	private string firstGuessPuzzle, secondGuessPuzzle;

	public bool started;

	public Canvas MainPuzzle;
	public Canvas Highscore;

	public Text ScoreText;
	public Text HighScoreText;




	public float ScoreCount;
	public static float HighScoreCount;

	public float pointsperSecond;

	public bool scoreIncreasing;

	public static int isiScores = 0;

	public static float timeLeft = 0.0f;

	public Text textHighScore;

	void Awake(){
		puzzles = Resources.LoadAll<Sprite> ("Sprites/puzzle");
	}
	void Start(){
		GetButtons();
		AddListeners ();
		AddGamePuzzles ();
		Shuffle (gamePuzzles);
		gameGuesses = gamePuzzles.Count / 2;
	}

	void GetButtons(){
		GameObject[] objects = GameObject.FindGameObjectsWithTag ("PuzzleButton");

		for(int i=0; i<objects.Length; i++){
			btns.Add(objects[i].GetComponent<Button>());
			btns [i].image.sprite = bgImage;
		}	
	}
	void AddGamePuzzles(){
		int looper = btns.Count;
		int index = 0;

		for (int i = 0; i < looper; i++) {
			if (index == looper / 2) {
				index = 0;
			}
			gamePuzzles.Add (puzzles [index]);

			index++;
		}
	}

	void AddListeners(){
		foreach (Button btn in btns) {
			btn.onClick.AddListener (() => PickAPuzzle ());
		}
	}
	public void PickAPuzzle(){
		string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;


		if (!firstGuess) {

			firstGuess = true;

			firstGuessIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);

			firstGuessPuzzle = gamePuzzles[firstGuessIndex].name;

			btns [firstGuessIndex].image.sprite = gamePuzzles [firstGuessIndex];
			if (!started) {
				started = true;
			}

		} else if (!secondGuess) {
			secondGuess = true;

			secondGuessIndex = int.Parse (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);

			secondGuessPuzzle = gamePuzzles [secondGuessIndex].name;

			btns [secondGuessIndex].image.sprite = gamePuzzles [secondGuessIndex];

			countGuesses++;

			StartCoroutine (CheckIfMatch());

		}


	}
		
		
	IEnumerator CheckIfMatch(){
		yield return new WaitForSeconds (1f);

		if (firstGuessPuzzle == secondGuessPuzzle) {

			yield return new WaitForSeconds (.5f);

			btns [firstGuessIndex].interactable = false;
			btns [secondGuessIndex].interactable = false;

			btns [firstGuessIndex].image.sprite= gamePuzzles [firstGuessIndex];
			btns [secondGuessIndex].image.sprite = gamePuzzles [secondGuessIndex];

			CheckIfFinish ();

		} else {

			yield return new WaitForSeconds (.5f);

			btns [firstGuessIndex].image.sprite = bgImage;
			btns [secondGuessIndex].image.sprite = bgImage;
		}

		yield return new WaitForSeconds (.5f);

		firstGuess = secondGuess = false;

	}
		
	void CheckIfFinish(){
		countCorrectGuesses++;

		if (countCorrectGuesses == gameGuesses) {
			Debug.Log ("Game Finish");
			Debug.Log ("it took you" + countGuesses + "many guess(es) to finish the game");

			if (HighScoreCount == 0) {
				HighScoreCount = ScoreCount;
			}else if (ScoreCount < HighScoreCount) {
					HighScoreCount = ScoreCount;
			}
		

			MainPuzzle.enabled = false;
			Highscore.enabled = true;

			if (isiScores == 0)
			{
				scores[0] = ScoreCount;
				isiScores++;
				Debug.Log (scores [0]);
			} else {
				for (int i = 0; i < isiScores; i++)
				{
					if (ScoreCount< scores[0])
					{
						for (int j = isiScores; j > 0; j--)
						{
							scores[j] = scores[j-1];
						}
						scores[0] = ScoreCount;
						break;
					}
					else if (ScoreCount > scores[isiScores - 1])
					{
						scores[isiScores] = ScoreCount;
					}
					else if (ScoreCount > scores[i] && ScoreCount < scores[i + 1])
					{
						for (int j = isiScores; j > i; j--)
						{
							scores[isiScores + 1] = scores[isiScores];
						}
						scores[i] = isiScores;
						break;
					}
				}
				isiScores++;
			}

			for (int j=0; j<isiScores; j++)
			{
				textHighScore.text = textHighScore.text + "\n" + Mathf.Round(scores[j]) + "\n";
			}

		}
	}


	void Shuffle(List<Sprite> list){

		for (int i = 0; i < list.Count; i++) {
			Sprite temp = list [i];
			int randomIndex = Random.Range (i, list.Count);
			list [i] = list [randomIndex];
			list [randomIndex] = temp;

		}

	}

	void Update () {
		if (started) {
			if (scoreIncreasing) {
				ScoreCount += pointsperSecond * Time.deltaTime;
		}

	
		//	ScoreCount += pointsperSecond * Time.deltaTime;
		}

		ScoreText.text = "Score : " + Mathf.Round(ScoreCount);
		HighScoreText.text = "High Score : " + Mathf.Round(HighScoreCount);

	}
	void swap(int a, int b){
		int temp = a;
		a = b;
		b = temp;

	}

}
